package ru.film.repo;


import org.springframework.data.repository.CrudRepository;
import ru.film.dto.User;

import java.util.List;

public interface UserRepo extends CrudRepository<User, Long> {

    List<User> findByName(String name);

    List<User> deleteByName(String name);

}
