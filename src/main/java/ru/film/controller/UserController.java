package ru.film.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.film.dto.User;
import ru.film.repo.UserRepo;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping({"/users"})
@Transactional
public class UserController {

    @Autowired
    UserRepo userRepo;

    private List<User> users = new ArrayList<>();

    @GetMapping(produces = "application/json")
    public List<User> firstPage() {
        return users;
    }


    @DeleteMapping(path = { "/{name}" })
    public User delete(@PathVariable("name") String name) {
        User deletedEmp = null;
//        userRepo.findAll().forEach(users::add);
        for (User emp : users) {
            if (emp.getName().equals(name)) {
                users.remove(emp);
                userRepo.deleteByName(emp.getName());
                deletedEmp = emp;
                break;
            }
        }
        return deletedEmp;
    }

    @PostMapping
    public User create(@RequestBody User user) {
        users.add(user);
        userRepo.save(user);
        return user;
    }




}

